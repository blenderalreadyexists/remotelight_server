#ifndef NETWORK_H
# define NETWORK_H

# define BUFFER_SIZE 1024

int proceed_client(int socket_base);

int init_network(int port);

int wait_client(int socket_base);

unsigned char *get_wsbytes(int socket);

int send_data(int socket, char *str, int size);

char *get_data(int socket, int size);

#endif // NETWORK_H
