#include <string.h>
#include <pthread.h>
#include "threads.h"
#include "network.h"
#include <stdio.h>
#include "wsframe.h"
#include "entities.h"
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "operations.h"

void *proceed_poll(void* s)
{
    struct entities *ent = (struct entities*) s;
    struct timeval tv;

    tv.tv_sec = 1;
    tv.tv_usec = 500000;		


    printf("now polling entities...\n");	


    while (1)
    {
        printf("Polling %d elements...\n", ent->n);
        //struct entities *copy = ent;
        int r = poll(ent->ufds, ent->n, 3500);
        if (r < 0)
            perror("select()");

        if (r == 0)
            printf("no data\n");
        else
        {	
            for (int i = 0; i < ent->n; i++)
            {
                if (ent->ufds[i].revents & POLLIN)
                {
                    //printf("got data on socket: %d \n", copy->ufds[i].fd);
                    char buf[256] = { 0 };
                    process_received_data(ent, i);	
                }
            }
        }
    }


}

int register_masters(struct entities *ent)
{
    int sk = init_network(20000);
    int id = 0;	

    while (1)
    {
        int master_socket = proceed_client(sk);
        ent = add_entity(ent, id++, master_socket, CLIENT_TYPE_MASTER);	
        
        struct entity_list* masterEntity = getEntityBySocket(master_socket, ent->ent_list);
        printf("got one master ! sending slaves to it...\n");
        process_RegAck(master_socket, MTYPE_SELF_REGACK, CLIENT_TYPE_MASTER, S_DOWN);
        send_slaves(ent, masterEntity);
    }

}

void *proceed_slaves(void *t)
{
    struct entities *ent = (struct entities*)t;
    register_slaves(ent);
    return NULL;


}

int register_slaves(struct entities *ent)
{
    int sk = init_network(10000);
    int id = 0;	
    //this function will call poll(..) in order to check if I/O operation has
    //to be performed.

    while (1)
    {
        int slave_socket = wait_client(sk); //ignore websocket stuff
        ent = add_entity(ent, id++, slave_socket, CLIENT_TYPE_SLAVE);

        struct entity_list* slaveEntity = getEntityBySocket(slave_socket, ent->ent_list);
        process_RegAck(slave_socket, MTYPE_SELF_REGACK, CLIENT_TYPE_SLAVE, S_DOWN);	
        printf("got one slave ! sending it to all masters by bc...\n");
        broadcast_slave(ent, slaveEntity);	
    }

}
