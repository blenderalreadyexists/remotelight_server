#ifndef BASE64_H
# define BASE64_H

int base64encode(const void* data_buf, size_t dataLength, char* result, size_t resultSize);

#endif // BASE64_H
