#ifndef ENTITIES_H
# define ENTITIES_H

#include <sys/select.h>
#include <sys/poll.h>

struct entity_list
{
    int id;
    int socket;	
    int role;	
    int status;
    struct entity_list *next;	


};



struct entities
{
    struct entity_list *ent_list;
    fd_set *fds;
    int n;
    //int last_socket;
    struct pollfd *ufds;
};




struct entities *add_entity(struct entities *ent, int id, int socket, int type);

struct entities *remove_entity(struct entities *ent, int index);


void free_entity_list(struct entity_list *el);



#endif // ENTITIES_H
