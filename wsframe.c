#include "wsframe.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pthread.h>


inline static struct wsframe *get_empty_ws(void)
{
	struct wsframe *wf = malloc(sizeof (struct wsframe));
	memset(wf, '\0', sizeof (struct wsframe));
	return wf;
}



struct wsframe *get_frame(int socket)
{
	int count = 2; //reprents the index after the payload_length part in the frame
	unsigned char *frame = get_wsbytes(socket);

	struct wsframe *wf = get_empty_ws();

	wf->fin = frame[0] >> 7 & 0x01;
	wf->rsv1 = frame[0] >> 6 & 0x01;
	wf->rsv2 = frame[0] >> 5 & 0x01;
	wf->rsv3 = frame[0] >> 4 & 0x01;
	wf->opcode = frame[0] & 0x0F;

	wf->masked = frame[1] >> 7 & 0x01;
	wf->payload_length = frame[1] & 0x7F;
	if (wf->payload_length == 126)
	{
		char *out = malloc(7);
		memset(out, '\0', 7);
		snprintf(out, 6, "%02X%02X", frame[2], frame[3]);
		wf->payload_length = strtol(out, NULL, 16);
		free(out);
		count = 4;
	}
	if (wf->payload_length == 127) {
		char *out = malloc(25);
		memset(out, '\0', 25);		
		snprintf(out, 24, "%02X%02X%02X%02X%02X%02X%02X%02X", frame[2], frame[3], frame[4], frame[5], 
																										 frame[6], frame[7], frame[8], frame[9]);
		wf->payload_length = strtol(out, NULL, 16);
		free(out);
		count = 10;
	}
	wf->mask[0] = frame[count];
	wf->mask[1] = frame[count+1];
	wf->mask[2] = frame[count+2];
	wf->mask[3] = frame[count+3];

	wf->masked_payload = malloc(sizeof (char) * wf->payload_length + 1);
	memset(wf->masked_payload, '\0', sizeof (char) * wf->payload_length + 1);
	for (unsigned int i = 0; i < wf->payload_length; i++)
		wf->masked_payload[i] = frame[count + 4 + i];

	printf("Payload of packet: %d\n", wf->payload_length);
	
        wf->unmasked_payload = malloc(sizeof (char) * wf->payload_length + 1);
	memset(wf->unmasked_payload, '\0', sizeof (char) * wf->payload_length + 1);
	for (unsigned int i = 0; i < wf->payload_length; i++)
		wf->unmasked_payload[i] = wf->masked_payload[i] ^ wf->mask[i % 4];

        
        if (!strcmp(wf->unmasked_payload, ""))
            wf->fin = 1;

	return wf;			
}


static inline int send_frame(int socket, char *data, int size, int opcode)
{
	char *frame = malloc(sizeof (char) * (10 + size)); //malloc'd
	memset(frame, '\0', size + 10);
	int count = 2;
	
	frame[0] = 1 << 7 | opcode; //1 corresponds to FIN byte
	frame[1] = size;

	if (size > 125 && size <= 65535)
	{
		frame[1] = 126;
		frame[2] = size >> 8;
		frame[3] = size;
		count = 4;
	}
	else if (size > 65535)
	{
		frame[9] = (size & 0xFF);
		frame[8] = (size & 0xFF00) >> 8;
		frame[7] = (size & 0xFF0000) >> 16;
		frame[6] = (size & 0xFF000000) >> 24;
		frame[5] = (size & 0xFF00000000) >> 32;
		frame[4] = (size & 0xFF0000000000) >> 40;
		frame[3] = (size & 0xFF000000000000) >> 48;
		frame[2] = (size & 0xFF00000000000000) >> 56;
		frame[1] = 127;
		
		count = 10;
	}

	memmove(&frame[count], data, size);
	int x = send_data(socket, frame, size + count );
	
	free(frame);
  	return x;
}



static inline struct wsframe *build_frame(int fin, int opcode, char *unmasked_payload, int size)
{
	struct wsframe *wf = get_empty_ws(); //malloc'd

	wf->fin = fin;
	wf->opcode = opcode;
	wf->payload_length = size;
	wf->unmasked_payload = malloc(sizeof (char) * size);//malloc'd
	memset(wf->unmasked_payload, '\0', sizeof (char) * size);

	memmove(&wf->unmasked_payload[0], &unmasked_payload[0], size);

	return wf;
}

inline static void freews(struct wsframe *wf)
{
	if (wf->unmasked_payload)
		free(wf->unmasked_payload);
	if (wf->masked_payload)
		free(wf->masked_payload);

	free(wf);
}

int send_dataws_fast(int socket, char *bytes, int size, int opcode)
{
	int x = send_frame(socket, bytes, size, opcode);
	return x;

}

int send_dataws(int socket, char *bytes, int size, int opcode)
{
	struct wsframe *wf = build_frame(1, opcode, bytes, size); //malloc'd
	
	int x = send_frame(socket, wf->unmasked_payload, wf->payload_length, wf->opcode); 	
	freews(wf);
  	return x;
}

char *get_dataws(int socket)
{

	char *data = NULL;
	unsigned int count = 0; //represents the number of payloads already passed += 127
	struct wsframe *wf = NULL;
	while (1)
	{
		wf = get_frame(socket);
		data = realloc(data, count + wf->payload_length + 1);
		
		memset(&data[count], '\0', wf->payload_length + 1);
		memmove(&data[count], &wf->unmasked_payload[0], wf->payload_length);

		count += wf->payload_length;

		if (wf->fin)
			break;
		free(wf);
	}
		free(wf);


	return data;
}
