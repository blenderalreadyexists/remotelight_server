#ifndef WSFRAME_H
# define WSFRAME_H

#include "network.h"

struct wsframe *get_frame(int socket);


int send_dataws(int socket, char *bytes, int size, int opcode);

//this function does not alloc a wsframe struct
int send_dataws_fast(int socket, char *data, int size, int opcode);

char  *get_dataws(int socket);

struct wsframe 
{
	int fin;
	int rsv1;
	int rsv2;
	int rsv3;

	int opcode;
	int masked;
	unsigned payload_length; //127 bytes max

	char mask[4];

	char *masked_payload;
	char *unmasked_payload;

};

#endif // WSFRAME_H
