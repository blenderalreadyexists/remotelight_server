#ifndef OPERATIONS_H
# define OPERATIONS_H

#include "entities.h" 

#define SOCKET_CONTROLLER 0

#define MTYPE_SELF_REGACK 0
#define MTYPE_OTHER_REGACK 1
#define MTYPE_REQUEST 2
#define MTYPE_REPLY 3

#define CLIENT_TYPE_SLAVE 0
#define CLIENT_TYPE_MASTER 1

#define S_DOWN 0
#define S_UP 1

struct ap
{
	char from;
	char to;
	char m_type;
	char c_type;
	char message;
};

int process_RegAck(int socket, char m_type, char client_type, char state);


int broadcast_slave(struct entities *ent, struct entity_list* slaveEntity);

struct ap *build_ap(char *str);

void send_ap(struct ap *a);

int process_received_data(struct entities *ent, int index);

int send_slaves(struct entities *ent, struct entity_list* masterEntity);

struct entity_list *getEntityBySocket(char socket, struct entity_list *ent);

struct entity_list *getItemN(struct entity_list *list, int n);

//int proceed_slave(int socket);

#endif // OPERATIONS_H
