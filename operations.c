#include "operations.h"
#include "network.h"
#include <stdlib.h>
#include <string.h>
#include "wsframe.h"
#include <stdio.h>

char *getStrFromParameters(char from, char to, char m_t, char c_t, char m)
{
    char *str = malloc(sizeof (char) * 5 + 1);
    memset(str, '\0', 6);

    str[0] = from + 48;
    str[1] = to + 48;
    str[2] = m_t + 48;
    str[3] = c_t + 48;
    str[4] = m + 48;
    return &str[0];

}



struct ap *build_ap(char *str)
{
    struct ap *ap = malloc (sizeof (struct ap));

    memset(ap, '\0', sizeof (struct ap));

    ap->from = str[0] - 48;
    ap->to = str[1] - 48;
    ap->m_type = str[2] - 48;
    ap->c_type = str[3] - 48;
    ap->message = str[4] - 48;

    return ap;
}

struct entity_list *getItemN(struct entity_list *list, int n)
{
	if (n < 0)
		return NULL; 
    if (n == 0)
        return list;
    
    for (int i = 0; i < n; i++)
		if (list != NULL)
			list = list->next;
		else
			return NULL;

    return list;
}
/*void send_ap(struct ap *a)
  {
  char *str = malloc(sizeof (char) * 5 + 1);
  memset(str, '\0', 5);

  str[0] = a->from;
  str[1] = a->to;
  str[2] = a->m_type;
  str[3] = a->c_type;
  str[4] = a->message;

  }*/

int broadcast_slave(struct entities *ent, struct entity_list* slaveEntity)
{
    struct entity_list *list = ent->ent_list;
    while (list != NULL)
    {
        if (list->role == CLIENT_TYPE_MASTER)
        {
            char *str = getStrFromParameters(slaveEntity->socket,
                    list->socket,
                    MTYPE_OTHER_REGACK, 
                    CLIENT_TYPE_SLAVE,
                    slaveEntity->status);
            send_dataws(list->socket, str, 5, 1);
        }

        list = list->next;
    }	

}

int send_slaves(struct entities *ent, struct entity_list* masterEntity)
{
    struct entity_list *list = ent->ent_list;

    while (list != NULL)//do not send other masters
    {
        if (list->role == CLIENT_TYPE_SLAVE)
        {
            char *str = getStrFromParameters(list->socket,
                    masterEntity->socket,
                    MTYPE_OTHER_REGACK, CLIENT_TYPE_MASTER, masterEntity->status);

            send_dataws(masterEntity->socket, str, 5, 1);
        }
        list = list->next;
    }

}

struct entity_list *getEntityBySocket(char socket, struct entity_list *ent)
{
    struct entity_list *c = ent;
    while (c != NULL && c->socket != socket)
        c = c->next;

    return c;

}



int process_RegAck(int socket, char m_type, char client_type, char state)
{
     char *str = getStrFromParameters(SOCKET_CONTROLLER, socket, m_type, 
            client_type, state);

    printf("%d\n", client_type);
   if (client_type == CLIENT_TYPE_SLAVE)
        send_data(socket, str, 5);
    if (client_type == CLIENT_TYPE_MASTER)
        send_dataws_fast(socket, str, 5, 1);

	free(str);

}

int process_Req(struct ap *a, struct entities *ent)
{
    //struct entity_list *el = getItemFromSocket(a->to, ent->ent_list); 
    char *str = getStrFromParameters(a->from, a->to, a->m_type, 
            a->c_type, a->message);


    printf("Sending Reply to %d\n", a->to);
    send_data(a->to, str, 5);
	free(str);
}

int process_ReqAck(struct ap *a, struct entities *ent)
{
    struct entity_list *el = getEntityBySocket(a->to, ent->ent_list);

    char *str = getStrFromParameters(a->from, a->to, a->m_type, 
            a->c_type, a->message);


    el->status = a->message;

    send_dataws(a->to, str, 5, 1);	

}
int process_received_data(struct entities *ent, int index)
{

    struct entity_list *el = getItemN(ent->ent_list, index);
    int socket = ent->ufds[index].fd;
    char *data = malloc(5);
    if (el->role == CLIENT_TYPE_SLAVE)
        data = get_data(socket, 5);
    else
        data = get_dataws(socket);
    printf("Got: %s\n", data);

    if (!strcmp(data, ""))
    {
        printf("Socket lost. Removing it.\n");
        ent = remove_entity(ent, index);        
		free(data);
		return 0;
	
	}

    struct ap *a = build_ap(data);

    if (a->m_type == MTYPE_REQUEST)
        process_Req(a, ent);
    if (a->m_type == MTYPE_REPLY)
        process_ReqAck(a, ent);

	free(data);
	free(a);
    //build core process here	
}


