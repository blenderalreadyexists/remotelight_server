#include "stream.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "base64.h"
#include "field.h"
#include <openssl/sha.h>

int send_data(int socket, char *str, int size)
{
    int s = send(socket, str, size, MSG_NOSIGNAL);
	printf("sending: %s\n", str);
    return s;
}

char *get_data(int socket, int size)
{
	char *buff = malloc(sizeof (char) * size + 1);
	memset(buff, '\0', sizeof (char) * size + 1);
	int r = recv(socket, buff, sizeof (char) * size, 0);
	
	return &buff[0];
		

}

int init_network(int port)
{

    int socket_base;
    struct sockaddr_in servaddr;

    socket_base = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_base < 0)
    {
        perror("socket");
        exit(2);
    }

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(port);

    int optval = 1;
    if (setsockopt(socket_base, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof (optval)) < 0)
    {
        perror("setsockopt");
        exit(8);
    }

    if (bind (socket_base, (struct sockaddr*)&servaddr,
                sizeof (servaddr)) < 0)
    {
        perror("bind");
        exit(3);
    }

    listen(socket_base, 5);

    return socket_base;
}

int wait_client(int socket_base)
{
    struct sockaddr_in cliaddr;
    socklen_t cl_ad = sizeof (cliaddr);

    int socket_client = accept(socket_base, (struct sockaddr*)&cliaddr, &cl_ad);

    if (socket_client < 0)
    {
        perror("accept");
        exit(5);
    }
    return socket_client;
}


static int deduce_wsframepayload(unsigned char *frame)
{

    int length = frame[1] & 0x7F;

    if (length == 126)
    {
        char *out = malloc(7);
        memset(out, '\0', 7);
        snprintf(out, 6, "%02X%02X", frame[2], frame[3]);
        length = strtol(out, NULL, 16);
        free(out);
    }
    if (length == 127) {
        char *out = malloc(25);
        memset(out, '\0', 25);		
        snprintf(out, 24, "%02X%02X%02X%02X%02X%02X%02X%02X", frame[2], frame[3], frame[4], frame[5], 
                frame[6], frame[7], frame[8], frame[9]);
        length = strtol(out, NULL, 16);
        free(out);
    }
    return length;
}


unsigned char *get_wsbytes(int socket)
{
    unsigned char *recv_buff = malloc(sizeof (unsigned char) * 10000);
    memset(recv_buff, '\0', sizeof (unsigned char) * 10000);

    int bytesread = 0;
    int torecv = 0;

    bytesread = read(socket, recv_buff, 10000);
    if (bytesread < 0)
        perror("recv");
    torecv = deduce_wsframepayload(recv_buff);
    torecv += 14; //header
    torecv -= 10000;

    printf("deducing that total packet length is: %d\n", torecv + bytesread);

    if (bytesread < torecv)
    {
        printf("data remaining...\n");
        recv_buff = realloc(recv_buff, bytesread + torecv + 1);

        memset(&recv_buff[bytesread], '\0', torecv + 1);

        int r = recv(socket, &recv_buff[bytesread], torecv, MSG_WAITALL);
        printf("additional bytes: %d\n", r);
        bytesread += r;
    }


    printf("packet size is: %d\n", bytesread);
    return &recv_buff[0];

}

int proceed_client(int socket_base)
{

    int socket = wait_client(socket_base);
    unsigned char *req = get_wsbytes(socket);

    char *pattern = "Sec-WebSocket-Key: ";
    unsigned char *key = get_field(req, pattern);

    char magic[37] = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11\0";
    int keysize = strlen((char*)key);
    char *newkey = realloc(key, keysize + strlen(magic) + 1);
    newkey = strncat(newkey, magic, strlen(magic));

    unsigned char sha[SHA_DIGEST_LENGTH] = { 0 };
    SHA1((unsigned char*)newkey, strlen(newkey), sha);


    int b64 = ((4 * SHA_DIGEST_LENGTH / 3) + 3) & ~ 3;
    char *base64 = malloc(sizeof (char) * b64 + 1); 
    memset(base64, '\0', b64 + 1);
    base64encode((unsigned char*)sha, SHA_DIGEST_LENGTH, base64, b64);

    char *resp = malloc(sizeof (char) * 150); 
    memset(resp, '\0', 150);

    sprintf(resp, "HTTP/1.1 101 Switching Protocols\r\n"
            "Upgrade: websocket\r\n"
            "Connection: Upgrade\r\n"
            "Sec-WebSocket-Accept: %s\r\n\r\n", base64);


    send_data(socket, resp, strlen(resp));

    free(req);
    free(newkey);
    free(base64);
    free(resp);
    
    return socket;
}
