#include "field.h"
#include <string.h>
#include <stdio.h>
int get_line_size(char *str)
{
if (str == NULL)
	return 0;
int i = 0;
while (str[i] != '\r')
{
	i += 1;
}

return i;
}

unsigned char *get_field(unsigned char* req, char *pattern)
{

	char *match = strstr((char*)req, pattern);
	if (match == NULL)
		return NULL;


	int pat_size = get_line_size(match) - strlen(pattern);

	unsigned char *res = malloc(pat_size + 1);
	memset(res, '\0', pat_size + 1);

	for (int i = 0; i < pat_size; i++)
		res[i] = match[strlen(pattern) + i];

	return res;
}
