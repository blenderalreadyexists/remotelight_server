CC= gcc
CFLAGS= -std=c99 -Werror -Wextra -pedantic -Wall -O3
SRC= operations.c stream.c entities.c network.c field.c base64.c wsframe.c threads.c

all:
	$(CC) $(CFLAGS) $(SRC) -o stream -lssl -lcrypto `pkg-config `

ez:
	gcc -pthread -std=c99 -g3 $(SRC) -o stream -lssl -lcrypto


