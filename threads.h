#ifndef THREADS_H
# define THREADS_H

#include "entities.h"

int register_masters(struct entities *ent);


int register_slaves(struct entities *ent);

void *proceed_poll(void* s);

void *proceed_slaves(void *t);

#endif // THREADS_H
