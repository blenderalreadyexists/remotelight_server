#define _BSD_SOURCE

#include <time.h>
#include "stream.h"
#include "network.h"
#include <string.h>
#include <stdio.h>
#include <openssl/sha.h>
#include "field.h"
#include "base64.h"
#include "wsframe.h"
#include <unistd.h>
#include "threads.h"
#include <pthread.h>

int main(int argc, char *argv[])
{
    int camera = -1;

    int socket = 0;
    int ok = 0;
	
	struct entities *ent = malloc(sizeof (struct entities));
	memset(ent, '\0', sizeof (struct entities));		 

	printf("Waiting for any client...\n");
    //socket = proceed_client(socket_base); //wait client + HTTP handshake
	//poll_master(0);

	pthread_t tpoll;	
	pthread_t tslaves;
	int poll = pthread_create(&tpoll, NULL, proceed_poll, ent);
	int slaves = pthread_create(&tslaves, NULL, proceed_slaves, ent);	

	register_masters(ent);
    
	printf("client joined !\n");
    getchar();
	return 0;
}
