#include <stdio.h>
#include <stdlib.h>
#include "entities.h"
#include "operations.h"
#include <sys/select.h>
#include <string.h>


struct entities *add_entity(struct entities *ent, int id, int socket, int
		type)
{
	struct entity_list *sl = ent->ent_list;
	struct entity_list *head = sl;

	while (head != NULL && head->next != NULL)
		head = head->next;

	struct entity_list *new_entity = malloc(sizeof (struct entity_list));
	memset(new_entity, '\0', sizeof (struct entity_list));	

	new_entity->id = id;
	new_entity->socket = socket;
	new_entity->role = type;
	new_entity->next = NULL;

	if (ent->fds == NULL)
	{
		ent->fds = malloc(sizeof (fd_set));
		FD_ZERO(ent->fds);	
	}	

	FD_SET(socket, ent->fds);
	if (head == NULL)
	{
		ent->ent_list = new_entity;
		ent->n = 1;
		//ent->last_socket = new_entity->socket;
		ent->ufds = malloc(sizeof (struct pollfd));
		ent->ufds[0].fd = new_entity->socket;
		ent->ufds[0].events = POLLIN;
		return ent;
	}
	else
		head->next = new_entity;

	ent->n += 1;
	//ent->last_socket = new_entity->socket;
	ent->ufds = realloc(ent->ufds, sizeof (struct pollfd) * ent->n);
	ent->ufds[ent->n-1].fd = new_entity->socket;
	ent->ufds[ent->n-1].events = POLLIN;
	return ent;



}


struct entities *remove_entity(struct entities *ent, int index)
{
	struct entity_list* pre_el = getItemN(ent->ent_list, index - 1);
	struct entity_list* post_el = getItemN(ent->ent_list, index + 1);

	ent->n -= 1;
	for (int i = index; i < ent->n ; i++)
		ent->ufds[i] = ent->ufds[i + 1];


	if (pre_el == NULL)
	{	

		ent->ufds = realloc(ent->ufds, sizeof (struct pollfd) * ent->n);
		struct entity_list *del = ent->ent_list;
		
		ent->ent_list = post_el;
		free(del);
		del = NULL;

	}
	else
	{

		ent->ufds = realloc(ent->ufds, sizeof (struct pollfd) * ent->n);	
		pre_el->next = post_el;

	}
	return ent;
}

/*void free_master_list(struct master_list *ml);

  void free_slave_list(struct slave_list *sl);
 */


