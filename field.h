#ifndef FIELD_H
# define FIELD_H

#include <unistd.h>
#include <stdlib.h>

unsigned char *get_field(unsigned char* req, char *pattern);


#endif // FIELD_H
